from django.test import TestCase,Client,LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import unittest
import time


# Create your tests here.

class TestJavaScriptPage(TestCase) :
    def test_url_ada(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_missing_url_return_404(self):
        c = Client()
        response = c.get("/apapun")
        self.assertEqual(response.status_code, 404)

    def test_html_status(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_ada_nama(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8')
        self.assertIn ("Azna Ajeng N", content)

# Kak, Functional Testnya jalan di local host tapi kalau di deploy ada tulisan "chrome failed to start exited abnormally deploy error"
# Terus gatau harus diapain lagi :(
        
# class FunctionalTest(unittest.TestCase):
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         # chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         service_log_path = "./chromedriver.log"
#         service_args = ['--verbose']
#         self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         self.browser.implicitly_wait(25)
#         super(FunctionalTest,self).setUp()

#     def tearDown(self):
#         self.browser.quit()
#         super(FunctionalTest, self).tearDown()

#     def test_dark_mode_light_mode(self):
#         self.browser.get('http://127.0.0.1:8000/')

#         time.sleep(5)
#         e = self.browser.find_element_by_css_selector(".toggle")
#         time.sleep(2)
#         e.click()
#         time.sleep(2)

#         light = "rgba(255, 255, 255, 1)"
#         dark = "rgba(128, 128, 128, 1)"


#         bg = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")
#         self.assertEqual(bg, dark)


#         e.click()
#         time.sleep(2)

#         bg = self.browser.find_element_by_tag_name("body").value_of_css_property("background-color")
#         self.assertEqual(bg, light)


#         time.sleep(2)






